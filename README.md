# Scivive

Scivive by Richard Heart

This is the edited book and audiobook mirror as of November 2022

# Links

- https://www.richardheart.com
- https://t.me/scivive
- https://www.youtube.com/watch?v=KsICwPAXwTU&list=PLzMKnu14mFRF4UjnaJskcDKXw0FuWAUhO
- http://scivive.net
- https://www.goodreads.com/book/show/49195282-scivive

# Commentary

- https://rhmax.org/blog/scivive-mind-body-spirit
- https://rhmax.org/blog/scivive-money-power-respect
- https://rhmax.org/blog/scivive-time-space-events-longevity
- https://rhmax.org/blog/fix-the-world
